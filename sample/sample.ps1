Import-Module Qlik-Cli
Import-Module qmiUtils.psm1 -Force

$currentUser=$env:UserName
#Use credentials of logged on user for authentication, prevents automatically locating a certificate
Write-Host "Authenticating in QlikSense with current logged on user: $currentUser"
Connect-Qlik -TrustAllCerts -UseDefaultCredentials


Import-QlikApp -file "$PSScriptRoot\Sales and Inventory Selections.qvf" -name "TestSelection" -upload | Out-Null
Import-QlikApp -file "$PSScriptRoot\Sales Details.qvf" -name "TestTemplate" -upload | Out-Null

 
#retentionTime values: "unlimited" | "P1D" (1 day) | "P0DT4H" (4 hours)

CreateOdagLink -user $currentUser -odagLinkName "testA" -selectionAppName "TestSelection" -detailsAppName "TestTemplate" -sheet2OpenName "Sales Details" -odagLinkExpression "count(Distinct ProductName)" -rowsLimit 20 -appsLimit 2 -retentionTime "P1D" -sheetEmbedName "Dashboard and Selection Sheet"
CreateOdagLink -user $currentUser -odagLinkName "testB" -selectionAppName "TestSelection" -detailsAppName "TestTemplate" -sheet2OpenName "Sales Details" -odagLinkExpression "count(Distinct ProductName)" -rowsLimit 150 -appsLimit 5 -retentionTime "P0DT4H" -sheetEmbedName "Dashboard and Selection Sheet"

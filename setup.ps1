 
Set-ExecutionPolicy Unrestricted
Write-Host "Installing Qlik-Cli-Windows..."
$PSVersionTable.PSVersion
Expand-Archive -LiteralPath "$PSScriptRoot\Qlik-Cli-Windows-1.21.0.zip" -DestinationPath "C:\Program Files\WindowsPowerShell\Modules\Qlik-Cli" -Force  | Out-Null
Get-ChildItem "C:\Program Files\WindowsPowerShell\Modules\Qlik-Cli\Qlik-Cli-Windows-1.21.0" | Copy-Item -Destination "C:\Program Files\WindowsPowerShell\Modules\Qlik-Cli" -Recurse -Force
Set-Location "C:\Program Files\WindowsPowerShell\Modules\Qlik-Cli"; Get-ChildItem -Recurse | Unblock-File
Import-Module Qlik-Cli
Import-Module .\Qlik-Cli.psd1

Set-Location $PSScriptRoot
Write-Host "Unzipping OdagLink EnigmaJS..."
Expand-Archive -LiteralPath "$PSScriptRoot\EnigmaModule.zip" -DestinationPath C:\OdagLinkNode -Force  | Out-Null

Write-Host "Importing QMI Utils module..." 
New-Item -ItemType directory -Path C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiUtils -force | Out-Null
Copy-Item $PSScriptRoot\qmiUtils.psm1 C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiUtils\qmiUtils.psm1 | Out-Null
Import-Module qmiUtils.psm1

Write-Host "Done setup."  

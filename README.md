# Create ODAG link Powershell function

## Setup

Download this code into the Qlik Sense server where the "odag link" creation will happen.

https://gitlab.com/qlik_gear/odag-link-create/-/archive/master/odag-link-create-master.zip

Then, run "setup.ps1" as an Administrator

This will install the following into the server:

- Powershell module Qlik-Cli-Windows (https://github.com/ahaydon/Qlik-Cli-Windows)
- Powershell module QMI-Utils that contains "CreateOdagLink" PS function
- NodeJS module using EnigmaJS in location C:\OdagLinkNode


## CreateOdagLink call example
```
CreateOdagLink -user $env:UserName -odagLinkName "testA" -selectionAppName "TestSelection" -detailsAppName "TestTemplate" -sheet2OpenName "Sales Details" -odagLinkExpression "count(Distinct ProductName)" -rowsLimit 20 -appsLimit 2 -retentionTime "P1D" -sheetEmbedName "Dashboard and Selection Sheet"
```

Params:
 - user: _the user that owns these applications_
 - odagLinkName: _the odag link name_
 - selectionAppName: _Selection App name_
 - detailsAppName: _Template App name_
 - sheet2OpenName: _Sheet to open when the generated app is created from the ODAG link_
 - odagLinkExpression: _The expresion f(x) to allow app generation_
 - rowsLimit: _Limit of rows for the app generation_
 - appsLimit: _Number of possible generated apps for this ODAG link_
 - retentionTime: _Retention time for generated apps (unlimited, P1D (1day), P0DT4H (4 hours), etc...)_
 - sheetEmbedName: _Sheet in the Selection App where to show/add this ODAG link into_

## Test

In folder "sample" there is a sample PS script. This sample code will connect to Qlik Sense in the same machine using the logged on user. It will import two test apps (Selection and Template app) and create a couple of ODAG links in the Selection App with the specified parameters:

From a PS terminal run "sample.ps1" script in "sample" folder.





<#
Module:             UtilsQMI
Author:             Manuel Romero
                    Clint Car
Modified by:        -
last updated:       11/10/2017
Modification History:
 -
Intent: One place for common functions across modules we don't want in qmiCLI
Dependencies:
 -
#>

Function CreateOdagLink
{
    Param (
        [string]$odagLinkName,
        [string]$selectionAppName,
        [string]$detailsAppName,
        [string]$sheet2OpenName,
        [string]$odagLinkExpression,
        [int]$rowsLimit,
        [int]$appsLimit,
        [string]$retentionTime,
        [string]$sheetEmbedName,
        [string]$user
    )


    PROCESS {

        Trap {
            Write-Host -ForegroundColor Red "CreateOdagLink# Error in function CreateOdagLink"
            Write-Host -ForegroundColor Red $_.Exception.Message
            Break
        }

        if ( -Not (Test-Path C:\OdagLinkNode\EnigmaModule) ) {
            Write-Host -ForegroundColor Yellow "CreateOdagLink# Necessary OdagLink EnigmaJS node module is missing."
        }

        # Create ODAG Link
        Write-Host "CreateOdagLink# Adding ODAG Link with Name: '$odagLinkName' ....."

        $templateApp = $(Get-QlikApp -filter "name eq '$detailsAppName'")
        $selectionApp = $(Get-QlikApp -filter "name eq '$selectionAppName'")

        if ( $templateApp -And $selectionApp ) {

            Write-Host "CreateOdagLink# Selection App Id $($selectionApp.id) - Template App Id $($templateApp.id)"

            $sheet2Open = Get-QlikObject -filter "name eq '$sheet2OpenName' and app.id eq $($templateApp.id) and objectType eq 'sheet'" -full
            
            if ($sheet2Open) {

                $sheet2OpenId = $($sheet2Open.engineObjectId);
                Write-Host "CreateOdagLink# Sheet to Open Id: $sheet2OpenId"

                $data = (@{
                    "id"="";
                    "name"=$odagLinkName;
                    "templateApp"=$($templateApp.id);
                    "selectionApp"=$($selectionApp.id);
                    "rowEstExpr"=$odagLinkExpression;
                    "privileges"=@();
                    "properties"=@{
                        "rowEstRange"=@(@{"context"="*";"highBound"=$rowsLimit});
                        "genAppLimit"=@(@{"context"="User_*";"limit"=$appsLimit});
                        "appRetentionTime"=@(@{"context"="User_*";"retentionTime"=$retentionTime});
                        "publishTo"=@();
                        "targetSheet"=@(@{"context"="User_*";"sheetId"=$sheet2OpenId})};
                    "tags"=@();
                }) | ConvertTo-Json -Compress -Depth 10
                    
                $result = $(Invoke-QlikPost "/api/odag/v1/links" $data)
                $odagLinkRef = $result.objectDef.id

                Write-Host "CreateOdagLink# ODAG link created, ref: $odagLinkRef"

                $sheetSelection = Get-QlikObject -filter "name eq '$sheetEmbedName' and app.id eq $($selectionApp.id) and objectType eq 'sheet'" -full

                if ($sheetSelection) {
                    $sheetSelectionID = $($sheetSelection.engineObjectId)
                
                    Write-Host "CreateOdagLink# Sheet Selection, ID: $sheetSelectionID"

                    $env:Path += "C:\Program Files\Qlik\Sense\ServiceDispatcher\Node\;"
                    node "C:\OdagLinkNode\EnigmaModule\index.js" $odagLinkRef $odagLinkName $($selectionApp.id) $sheetSelectionID $env:computername $user

                    Write-Host "CreateOdagLink# Done!!"

                    return $odagLinkRef

                } else {
                    Write-Host -ForegroundColor Yellow "CreateOdagLink# Not found sheet named '$sheetEmbedName' in specified Selection App."
                    return;
                }
            } else {
                Write-Host -ForegroundColor Yellow "CreateOdagLink# Not found sheet named '$sheet2OpenName' in specified Template App."
                return;
            }

        } else {
            Write-Host -ForegroundColor Yellow "CreateOdagLink# Not found in QS: Template App ($detailsAppName) or Selection App ($selectionAppName)"
            return;
        }
    }
}